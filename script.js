//Deklarasi dan Manipulasi Array
//Array  tiap kata dari nama lengkap
const nama = ["Rizky", "Tri", "Sulistyo"];
console.log(nama);

//Array umur
const umur = [22];
console.log(umur);

//Menggabungkan nama dengan umur
const gabung = nama.concat(umur);
console.log(gabung);

//Menambahkan tahun di antara nama dan umur
gabung.splice(3, 0, 1998);
console.log(gabung);

//Fungsi menampilkan setiap elemen yang ada dalam Array
const isiArray = (nArray) => {
  for (let iterasi = 0; iterasi < nArray.length; iterasi++) {
    let element = nArray[iterasi];
    console.log(element);
  }
};

//Memanggil Fungsi
isiArray(gabung);

//Penggunaan function di Javascript dan HTML
//Deklarasi variabel dengan id pada html
let input = document.getElementById("input");

//fungsi tambah kurang untuk onclick
const tambahKurang = (n) => {
  hasil = parseInt(input.value) + n;
  input.value = hasil;
};

//konversi suhu fahrenheit dengan Celcius
//fungsi Fahrenheit ke Celcius
let input_suhu = document.getElementById("nilai_suhu");
let tombol_ubah = document.getElementById("tombol_ubah");
let hasil_suhu = document.getElementById("hasil_suhu");

tombol_ubah.onclick = () => {
  ubahSuhuFkeC(input_suhu.value);
  tombol_ubah.innerHTML = "Ubah ke Fahrenheit";
};

//fungsi Fahrenheit ke Celcius
const ubahSuhuFkeC = (fahrenheit) => {
  hasil = (5 / 9) * (fahrenheit - 32);
  hasil_suhu.innerHTML = hasil ;
};

//fungsi Celcius ke Fahrenheit
const ubahSuhuCkeF = (celcius) => {
  hasil = (9 / 5) * (celcius + 32);
  hasil_suhu.innerHTML = hasil;
};
